import 'package:flutter/material.dart';

void showError(BuildContext context, String error){
  showDialog(context: context, builder: (BuildContext context){
    return AlertDialog(
      title: Text('Error'),
      content: Text(error),
      actions: [
        OutlinedButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Ok'))
      ],
    );
  });
}

