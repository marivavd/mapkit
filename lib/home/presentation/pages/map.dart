import 'dart:async';

import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:flutter/material.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});




  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late final YandexMapController _mapController;
  Completer<YandexMapController> _completer = Completer();

  var _mapZoom = 0.0;

  /// Данные о местоположении пользователя
  CameraPosition? _userLocation;

  /// Список точек на карте, по которым строится выделенная область
  List<Point> _polygonPointsList = [
    
  ];

  /// Список точек на карте, по которым строится автомобильный маршрут
  List<Point> _drivingPointsList = [];

  /// Результаты поиска маршрутов на карте
  DrivingResultWithSession? _drivingResultWithSession;

  /// Список объектов линий на карте, которые отображают маршруты
  List<PolylineMapObject> _drivingMapLines = [];

  @override
  void dispose() {
    _mapController.dispose();
    _drivingResultWithSession?.session.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: YandexMap(
        zoomGesturesEnabled: true,
        onMapCreated: _onMapCreated,
        mapObjects: [
        PolygonMapObject(
          strokeColor: Colors.red,
          strokeWidth: 3.0,
          fillColor: Colors.red.withOpacity(0.2),
        mapId: const MapObjectId('heart'),
        polygon: Polygon(
            outerRing: LinearRing(
                points: [
              Point(latitude: 60, longitude: 50),
              Point(latitude: 65, longitude: 60),
              Point(latitude: 60, longitude: 70),
              Point(latitude: 65, longitude: 80),
              Point(latitude: 60, longitude: 90),
              Point(latitude: 40, longitude: 70),
            ],
            ),
            innerRings: [
            ],

        ),
      ),
          PolygonMapObject(
            strokeColor: Colors.deepPurpleAccent,
            strokeWidth: 3.0,
            mapId: const MapObjectId('raccoon'),
            polygon: Polygon(
              outerRing: LinearRing(
                points: [
                  Point(latitude: 40, longitude: 140),
                  Point(latitude: 50, longitude: 120),
                  Point(latitude: 60, longitude: 130),
                  Point(latitude: 60, longitude: 150),
                  Point(latitude: 50, longitude: 160),
                ],
              ),
              innerRings: [
                LinearRing(points: [
                  Point(latitude: 55, longitude: 130),
                  Point(latitude: 57, longitude: 133),
                  Point(latitude: 55, longitude: 136),
                ],),
                LinearRing(points: [
                  Point(latitude: 55, longitude: 150),
                  Point(latitude: 57, longitude: 147),
                  Point(latitude: 55, longitude: 144),
                ],),
                LinearRing(points: [
                  Point(latitude: 42, longitude: 138),
                  Point(latitude: 44, longitude: 140),
                  Point(latitude: 42, longitude: 142),
                  Point(latitude: 40, longitude: 140),
                ],),
              ],

            ),
          ),
          PolygonMapObject(
            strokeColor: Colors.deepPurpleAccent,
            strokeWidth: 3.0,
            mapId: const MapObjectId('ear 1 of raccoon'),
            polygon: Polygon(
              outerRing: LinearRing(
                points: [
                  Point(latitude: 58, longitude: 125),
                  Point(latitude: 63, longitude: 125),
                  Point(latitude: 60, longitude: 135),
                ],
              ),
              innerRings: [
              ],

            ),
          ),
          PolygonMapObject(
            strokeColor: Colors.deepPurpleAccent,
            strokeWidth: 3.0,
            mapId: const MapObjectId('ear 2 of raccoon'),
            polygon: Polygon(
              outerRing: LinearRing(
                points: [
                  Point(latitude: 58, longitude: 155),
                  Point(latitude: 63, longitude: 155),
                  Point(latitude: 60, longitude: 145),
                ],
              ),
              innerRings: [
              ],

            ),
          ),


        ]
      )
    );
  }

  void _onMapCreated(YandexMapController controller){
    _completer.complete(controller);

  }

}
