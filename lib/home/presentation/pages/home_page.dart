import 'package:mapkit/home/presentation/pages/map.dart';
import 'package:mapkit/home/presentation/pages/my_coords.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});




  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 100,),
            OutlinedButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));}, child: Text('Map')),
            SizedBox(height: 100,),
            OutlinedButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Coords_Page()));}, child: Text('My coords'))
          ],
        ),
      )
       
    );
  }
}
