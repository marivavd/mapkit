import 'dart:async';

import 'package:mapkit/home/domain/app_lat_log.dart';
import 'package:mapkit/home/domain/location_service.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:flutter/material.dart';

class Coords_Page extends StatefulWidget {
  const Coords_Page({super.key});




  @override
  State<Coords_Page> createState() => _Coords_PageState();
}

class _Coords_PageState extends State<Coords_Page> {
  Completer<YandexMapController> _completer = Completer();


  @override
  void initState() {
    super.initState();
    _initPermission().ignore();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: YandexMap(
        zoomGesturesEnabled: true,
        onMapCreated: _onMapCreated,
      ),
    );
  }

  void _onMapCreated(YandexMapController controller){
    _completer.complete(controller);

  }

  Future<void> _initPermission() async {
    if (!await LocationService().checkPermission()) {
      await LocationService().requestPermission();
    }
    await _fetchCurrentLocation();
  }

  Future<void> _fetchCurrentLocation() async {
    AppLatLong location;
    const defLocation = MoscowLocation();
    try {
      location = await LocationService().getCurrentLocation();
    } catch (_) {
      location = defLocation;
    }
    _moveToCurrentLocation(location);
  }


  Future<void> _moveToCurrentLocation(
      AppLatLong appLatLong,
      ) async {
    (await _completer.future).moveCamera(
      animation: const MapAnimation(type: MapAnimationType.linear, duration: 1),
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: Point(
            latitude: appLatLong.lat,
            longitude: appLatLong.long,
          ),
          zoom: 12,
        ),
      ),
    );
  }
}
