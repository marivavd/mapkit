import 'package:flutter/material.dart';
import 'package:mapkit/auth/presntation/pages/register.dart';
import 'package:mapkit/core/theme.dart';
import 'package:mapkit/home/presentation/pages/home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      home: const Sign_up_Page(),
    );
  }
}
